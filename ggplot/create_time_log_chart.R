#!/usr/bin/Rscript

# Script for producing used-time time diagram

library(ggplot2)

args = commandArgs(TRUE)
input_file = args[1]
out_path = args[2]

header = readLines(input_file, n=1)
table = read.delim(input_file, sep="\t", skip=1)

tot_time = sum(table$ElapsedTime)
plot_threshold = tot_time / 100

filtered_table = table[table$ElapsedTime > plot_threshold, ]

header = sprintf("%s, Total running time: %.1fs", header, tot_time)

pdf(out_path)
filtered_table$Program = factor(filtered_table$Program, levels=rev(levels(filtered_table$Program)), ordered=TRUE)
plot = ggplot(data=filtered_table, aes(x=factor(Program, levels=unique(Program)),
    y=ElapsedTime, fill=Program), ordered=TRUE)
theme = theme(axis.text.x = element_text(angle=90, hjust=1))
plot + geom_bar(stat="identity", width=0.8) + ggtitle(header) + theme

