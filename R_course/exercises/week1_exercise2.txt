Exercise2 workflow

GUD34 = read.csv("GUD34.csv", sep=";")
GUD56 = read.csv("GUD56.csv", sep=";")

GUD36 = rbind(GUD34, GUD56) 
<- "row-bind", binds frames by rows, cbind binds by columns

aveGUD = aggregate(GUD36$gud, by=list(GUD36$pid), mean)
<- So, we apply function mean to groups of values defined by the
'by' value. This is applied on the $gud column.
The 'by' argument only takes a list, therefore the 'list()'

colnames(aveGUD) = c("pid", "gud")
-> Back-renaming of columns to "pid" and "gud"

Fledge = read.csv("FLEDGE.csv", sep=";")
aveFLE = aggregate(Fledge$fledge, by=list(Fledge$pid), mean)
colnames(aveFLE) <- c("pid", "fledge")

# Merge command! Building together part of R-frames
# Joining two data frames by one or more common key variables
# (defined by 'by')

merged = merge(aveGUD, aveFLE, by.y="pid", all=TRUE)

# If not all=TRUE, it filters columns where only part of the data
# is present.

reg = lm(merged$fledge~merged$gud)

# Plotting the number of fledgings against gud
# GUD: A food density in area when birds will start searching
# for new areas.

summary(reg)

# Interesting info about the regression

plot(merged$fledge, merged$gud)
